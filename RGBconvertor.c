#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define MAX_LIMIT 100

char *RemoveWhiteSpaces(char *strin)
{
    int i = 0, j = 0;
	while (strin[i])
	{
		if (strin[i] != ' ')
          strin[j++] = strin[i];
		i++;
	}
	strin[j] = '\0';
    i = 0;
    j = 0;
	while (strin[i])
	{
		if (strin[i] != '\t')
          strin[j++] = strin[i];
		i++;
	}
	strin[j] = '\0';
        i = 0;
    j = 0;
	while (strin[i])
	{
		if (strin[i] != ')')
          strin[j++] = strin[i];
		i++;
	}
	strin[j] = '\0';
	return strin;
}

int main()
{
    char inputString[MAX_LIMIT];
    printf("Zadejte barvu v RGB formatu:\n");
    fgets(inputString, MAX_LIMIT, stdin);
    RemoveWhiteSpaces(inputString);
    char splitBy1[] = "(";
    char *prefix = strtok(inputString, splitBy1);
    char *body = strtok(NULL, splitBy1);
    if (strcmp(prefix, "rgb") != 0)
    {
        printf("Nespravny vstup.\n");
        return 1;
    }
    int r = -1;
    int g = -1;
    int b = -1;
    char *splitBy2 = strtok(body, ","); 
    int size = atoi(splitBy2);
    int signal[size];
    int i = 0;
    while (splitBy2 != NULL) 
    { 
        if (i == 0)
            r = atoi(splitBy2);
        else if (i == 1)
            g = atoi(splitBy2);
        else
            b = atoi(splitBy2);
        splitBy2 = strtok(NULL, ","); 
        if(splitBy2 != NULL)
        {
            signal[i] = atoi(splitBy2);
            i++;
        }
    }
    if (r < 0 || r > 255 || g < 0 || g > 255 || b < 0 || b > 255)
    {
        printf("Nespravny vstup.\n");
        return 1;
    }
    char out[] = {'0', '0', '0', '0', '0', '0'};
    int to0 = r / 16;
    int to1 = r % 16;
    switch (to0)
    {
    case 0:
        out[0] = '0';
        break;
    case 1:
        out[0] = '1';
        break;
    case 2:
        out[0] = '2';
        break;
    case 3:
        out[0] = '3';
        break;
    case 4:
        out[0] = '4';
        break;
    case 5:
        out[0] = '5';
        break;
    case 6:
        out[0] = '6';
        break;
    case 7:
        out[0] = '7';
        break;
    case 8:
        out[0] = '8';
        break;
    case 9:
        out[0] = '9';
        break;
    case 10:
        out[0] = 'A';
        break;
    case 11:
        out[0] = 'B';
        break;
    case 12:
        out[0] = 'C';
        break;
    case 13:
        out[0] = 'D';
        break;
    case 14:
        out[0] = 'E';
        break;
    case 15:
        out[0] = 'F';
        break;
    }
    switch (to1)
    {
    case 0:
        out[1] = '0';
        break;
    case 1:
        out[1] = '1';
        break;
    case 2:
        out[1] = '2';
        break;
    case 3:
        out[1] = '3';
        break;
    case 4:
        out[1] = '4';
        break;
    case 5:
        out[1] = '5';
        break;
    case 6:
        out[1] = '6';
        break;
    case 7:
        out[1] = '7';
        break;
    case 8:
        out[1] = '8';
        break;
    case 9:
        out[1] = '9';
        break;
    case 10:
        out[1] = 'A';
        break;
    case 11:
        out[1] = 'B';
        break;
    case 12:
        out[1] = 'C';
        break;
    case 13:
        out[1] = 'D';
        break;
    case 14:
        out[1] = 'E';
        break;
    case 15:
        out[1] = 'F';
        break;
    }

    to0 = g / 16;
    to1 = g % 16;
    switch (to0)
    {
    case 0:
        out[2] = '0';
        break;
    case 1:
        out[2] = '1';
        break;
    case 2:
        out[2] = '2';
        break;
    case 3:
        out[2] = '3';
        break;
    case 4:
        out[2] = '4';
        break;
    case 5:
        out[2] = '5';
        break;
    case 6:
        out[2] = '6';
        break;
    case 7:
        out[2] = '7';
        break;
    case 8:
        out[2] = '8';
        break;
    case 9:
        out[2] = '9';
        break;
    case 10:
        out[2] = 'A';
        break;
    case 11:
        out[2] = 'B';
        break;
    case 12:
        out[2] = 'C';
        break;
    case 13:
        out[2] = 'D';
        break;
    case 14:
        out[2] = 'E';
        break;
    case 15:
        out[2] = 'F';
        break;
    }
    switch (to1)
    {
    case 0:
        out[3] = '0';
        break;
    case 1:
        out[3] = '1';
        break;
    case 2:
        out[3] = '2';
        break;
    case 3:
        out[3] = '3';
        break;
    case 4:
        out[3] = '4';
        break;
    case 5:
        out[3] = '5';
        break;
    case 6:
        out[3] = '6';
        break;
    case 7:
        out[3] = '7';
        break;
    case 8:
        out[3] = '8';
        break;
    case 9:
        out[3] = '9';
        break;
    case 10:
        out[3] = 'A';
        break;
    case 11:
        out[3] = 'B';
        break;
    case 12:
        out[3] = 'C';
        break;
    case 13:
        out[3] = 'D';
        break;
    case 14:
        out[3] = 'E';
        break;
    case 15:
        out[3] = 'F';
        break;
    }

    to0 = b / 16;
    to1 = b % 16;
    switch (to0)
    {
    case 0:
        out[4] = '0';
        break;
    case 1:
        out[4] = '1';
        break;
    case 2:
        out[4] = '2';
        break;
    case 3:
        out[4] = '3';
        break;
    case 4:
        out[4] = '4';
        break;
    case 5:
        out[4] = '5';
        break;
    case 6:
        out[4] = '6';
        break;
    case 7:
        out[4] = '7';
        break;
    case 8:
        out[4] = '8';
        break;
    case 9:
        out[4] = '9';
        break;
    case 10:
        out[4] = 'A';
        break;
    case 11:
        out[4] = 'B';
        break;
    case 12:
        out[4] = 'C';
        break;
    case 13:
        out[4] = 'D';
        break;
    case 14:
        out[4] = 'E';
        break;
    case 15:
        out[4] = 'F';
        break;
    }
    switch (to1)
    {
    case 0:
        out[5] = '0';
        break;
    case 1:
        out[5] = '1';
        break;
    case 2:
        out[5] = '2';
        break;
    case 3:
        out[5] = '3';
        break;
    case 4:
        out[5] = '4';
        break;
    case 5:
        out[5] = '5';
        break;
    case 6:
        out[5] = '6';
        break;
    case 7:
        out[5] = '7';
        break;
    case 8:
        out[5] = '8';
        break;
    case 9:
        out[5] = '9';
        break;
    case 10:
        out[5] = 'A';
        break;
    case 11:
        out[5] = 'B';
        break;
    case 12:
        out[5] = 'C';
        break;
    case 13:
        out[5] = 'D';
        break;
    case 14:
        out[5] = 'E';
        break;
    case 15:
        out[5] = 'F';
        break;
    }
    out[strlen(out)-3] = '\0';
    printf("#%s\n", out);
    return 0;
}